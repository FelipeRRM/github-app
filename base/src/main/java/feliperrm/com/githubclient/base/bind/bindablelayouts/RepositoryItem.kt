package feliperrm.com.githubclient.base.bind.bindablelayouts

import android.view.View
import feliperrm.com.githubclient.R
import feliperrm.com.githubclient.base.bind.adapter.DataBindingAdapter

/**
 * Created by FelipeRRM on 20/11/2018.
 */
class RepositoryItem(
    val title: String?,
    val ownerName: String?,
    val isPrivate: Boolean,
    val lastPush: String?,
    val createdAt: String?,
    val id: Long,
    val clickedCallback: (clickedView: View, repositoryId: Long) -> Unit
) :
    DataBindingAdapter.LayoutViewModel(R.layout.repository_item) {

    fun onClicked(view: View) {
        clickedCallback.invoke(view, id)
    }
}