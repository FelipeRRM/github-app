package feliperrm.com.githubclient.base.utils

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableList
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import feliperrm.com.githubclient.base.App
import feliperrm.com.githubclient.base.bind.adapter.DataBindingAdapter
import feliperrm.com.githubclient.base.bind.adapter.DataBindingCallback
import feliperrm.com.githubclient.base.image.CustomImageView
import feliperrm.com.githubclient.base.image.configurators.UriConfigurator
import feliperrm.com.githubclient.base.image.configurators.ViewConfigurator
import feliperrm.com.githubclient.base.image.options.CustomCircularImageOptions

/**
 * Created by FelipeRRM on 22/11/2018.
 */

/**
 * Sets a list of bindable items
 */
@BindingAdapter("bindableItems", "lifecycleOwner", "bindCallback", requireAll = false)
fun RecyclerView.bindableItems(
    listItems: ObservableList<out DataBindingAdapter.LayoutViewModel>? = null,
    lifecycleOwner: LifecycleOwner? = null,
    callback: DataBindingCallback? = null
) {
    if (listItems != null) this.adapter =
            DataBindingAdapter(listItems, lifecycleOwner, callback) else this.visibility =
            View.GONE
}

/**
 * Sets the shrink animation on touch using data binding. The parameters are optional and will fallback to the default values if not set.
 * The target view is the same touch view unless explicitly defined.
 */
@BindingAdapter("shrinkTouchTarget", "shrinkTouchEndSize", "shrinkTouchDuration", requireAll = false)
fun View.setShrinkTouchAnim(targetView: View?, endSize: Float?, animDuration: Long?) {
    this.setOnTouchListener(
        AnimationUtils.getShrinkAnimation(
            targetView
                ?: this, endSize ?: 0.95f, animDuration
                ?: 250
        )
    )
}


@BindingAdapter("circularImageUrl")
fun CustomImageView.loadCircularImageUrl(url: String?) {
    App.imageService.load(
        UriConfigurator(url, ImageView.ScaleType.CENTER_CROP),
        ViewConfigurator(this, false, CustomCircularImageOptions(Color.WHITE, 8f, 0f))
    )
}