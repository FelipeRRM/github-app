package feliperrm.com.githubclient.base.image.postprocessors;

/**
 * Created by felipemagalhaes on 2017-10-31.
 */

public class CustomMultiplyPostprocessor implements CustomBasePostprocessor {

    public final static String ANTI_ALIAS_TO_WHITE_TRANSFORMATION = "#FFFFFF";

    private final String mColor;

    public CustomMultiplyPostprocessor(String mColor) {
        this.mColor = mColor;
    }

    public String getColor() {
        return mColor;
    }
}
