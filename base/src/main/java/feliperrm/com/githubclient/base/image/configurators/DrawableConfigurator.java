package feliperrm.com.githubclient.base.image.configurators;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by felipemagalhaes on 2017-09-29.
 * <p>
 * Class configuring a drawable resource ID and a scaling type.
 * An instance of this class can be used to configure a place holder or an error image, for example.
 * The scaling type will be applied to the Custom Image View when this resource is set as the image source.
 */

public class DrawableConfigurator {

    private int resId;
    @Deprecated
    private Drawable drawable;
    private ImageView.ScaleType scaleType;

    public DrawableConfigurator() {
    }

    public DrawableConfigurator(int resId) {
        this.resId = resId;
    }

    public DrawableConfigurator(int resId, ImageView.ScaleType scaleType) {
        this.resId = resId;
        this.scaleType = scaleType;
    }


    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public ImageView.ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    /**
     * Drawable is not properly used in all methods inside the Image Service. And this is kept only for backwards compatibility. Currently only {@link feliperrm.com.githubclient.base.image.ImageService#load(DrawableConfigurator, ViewConfigurator)} can use it.
     * Design your methods to work with {@link #resId} instead.
     *
     * @deprecated Use {@link #getResId()} instead.
     */
    @Deprecated
    public Drawable getDrawable() {
        return drawable;
    }

    /**
     * Drawable is not properly used in all methods inside the Image Service. Currently only {@link feliperrm.com.githubclient.base.image.ImageService#load(DrawableConfigurator, ViewConfigurator)} can use it.
     * Other methods will rely only on the {@link #resId} to work.
     *
     * @deprecated Use {@link #setResId(int)} instead.
     */
    @Deprecated
    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
