package feliperrm.com.githubclient.base.image.implementations;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.imagepipeline.request.Postprocessor;
import feliperrm.com.githubclient.BuildConfig;
import feliperrm.com.githubclient.R;
import feliperrm.com.githubclient.base.App;
import feliperrm.com.githubclient.base.image.CustomImageView;
import feliperrm.com.githubclient.base.image.ImageService;
import feliperrm.com.githubclient.base.image.callbacks.CustomBitmapCallback;
import feliperrm.com.githubclient.base.image.callbacks.CustomImageCallback;
import feliperrm.com.githubclient.base.image.configurators.DrawableConfigurator;
import feliperrm.com.githubclient.base.image.configurators.UriConfigurator;
import feliperrm.com.githubclient.base.image.configurators.ViewConfigurator;
import feliperrm.com.githubclient.base.image.options.CustomCircularImageOptions;
import feliperrm.com.githubclient.base.image.options.CustomResizeOptions;
import feliperrm.com.githubclient.base.image.postprocessors.*;
import feliperrm.com.githubclient.base.image.postprocessors.fresco.MultiplyPostprocessor;
import feliperrm.com.githubclient.base.image.postprocessors.fresco.ScalingBlurPostprocessor;
import timber.log.Timber;

/**
 * Created by felipemagalhaes on 2017-10-23.
 */
public final class FrescoImageService implements ImageService {

    private static final boolean isDebug = BuildConfig.DEBUG;

    private static boolean isInitialized = false;

    private static final int DEFAULT_IMAGE_PLACEHOLDER_ID = R.drawable.image_placeholder;


    public FrescoImageService() {
    }

    /**
     * Inits Image Loading. This should be only called once!
     *
     * @param context
     */
    public void init(Context context) {
        if (isInitialized) {
            return;
        }
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(context)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(context, config);
        isInitialized = true;
    }

    /**
     * Loads a resource into a Simple Drawee View.
     *
     * @param drawableConfigurator
     * @param container
     */
    public void load(DrawableConfigurator drawableConfigurator, CustomImageView container) {
        load(drawableConfigurator, new ViewConfigurator(container));
    }

    /**
     * Loads a resource into a Simple Drawee View.
     *
     * @param drawableConfigurator
     * @param viewConfigurator
     */
    public void load(DrawableConfigurator drawableConfigurator, ViewConfigurator viewConfigurator) {
        CustomImageView container = viewConfigurator.getImageView();
        if (container != null) {
            RoundingParams roundingParams = getFrescoRoundingParams(viewConfigurator.getCircularImageOptions());
            if (roundingParams != null) {
                container.getHierarchy().setRoundingParams(roundingParams);
            }
            if (drawableConfigurator.getScaleType() != null) {
                container.getHierarchy().setActualImageScaleType(getFrescoScaleType(drawableConfigurator.getScaleType(), container));
            }
            if (drawableConfigurator.getDrawable() != null) {
                container.setImageDrawable(drawableConfigurator.getDrawable());
                container.getHierarchy().setPlaceholderImage(drawableConfigurator.getDrawable());
            } else {
                ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithResourceId(drawableConfigurator.getResId());
                container.setImageResource(drawableConfigurator.getResId());
                PipelineDraweeControllerBuilder builder = Fresco.newDraweeControllerBuilder();
                builder.setImageRequest(requestBuilder.build());
                container.setController(builder.build());
            }
        }
    }

    /**
     * Loads an image from an URL and invokes the provided callback.
     *
     * @param context
     * @param url
     * @param callback
     */
    public void load(Context context, String url, CustomBitmapCallback callback) {
        load(context, Uri.parse(url), callback);
    }

    /**
     * Loads an image from an URI and invokes the provided callback.
     *
     * @param context
     * @param uri
     * @param callback
     */
    public void load(Context context, Uri uri, final CustomBitmapCallback callback) {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();

        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(uri)
                .setRequestPriority(Priority.HIGH)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();

        DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, context);

        final BaseBitmapDataSubscriber frescoCallback = new BaseBitmapDataSubscriber() {
            @Override
            protected void onNewResultImpl(Bitmap bitmap) {
                if (callback != null) {
                    callback.onBitmapLoaded(bitmap);
                }
            }

            @Override
            protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                if (callback != null) {
                    callback.onLoadFailed();
                }
            }

            @Override
            public void onCancellation(final DataSource<CloseableReference<CloseableImage>> dataSource) {
                Timber.d("onCancellation");
            }
        };

        dataSource.subscribe(frescoCallback, CallerThreadExecutor.getInstance());
    }

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View.
     *
     * @param imageConfigurator object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param viewConfigurator  object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator imageConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(imageConfigurator.getUri(),
                imageConfigurator.getResizeOptions(),
                imageConfigurator.getPostprocessor(),
                imageConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                null,
                null,
                null,
                null,
                null, null, null);
    }

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View, determining the behavior of the placeholder and the error state.
     *
     * @param imageConfigurator       object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator imageConfigurator,
                     DrawableConfigurator placeholderConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(imageConfigurator.getUri(),
                imageConfigurator.getResizeOptions(),
                imageConfigurator.getPostprocessor(),
                imageConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                placeholderConfigurator.getResId(),
                placeholderConfigurator.getScaleType(),
                null,
                null,
                null, null, null);
    }

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View, determining the behavior of the placeholder and the error state.
     *
     * @param imageConfigurator       object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param errorConfigurator       object configuring the error state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator imageConfigurator,
                     DrawableConfigurator placeholderConfigurator,
                     DrawableConfigurator errorConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(imageConfigurator.getUri(),
                imageConfigurator.getResizeOptions(),
                imageConfigurator.getPostprocessor(),
                imageConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                placeholderConfigurator.getResId(),
                placeholderConfigurator.getScaleType(),
                errorConfigurator.getResId(),
                errorConfigurator.getScaleType(),
                null, null, null);
    }

    /**
     * @param highResConfigurator object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator  object wrapping the URL and configurations that will affect the image when it is loaded.
     *                            This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param viewConfigurator    object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator highResConfigurator,
                     UriConfigurator lowResConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(highResConfigurator.getUri(),
                highResConfigurator.getResizeOptions(),
                highResConfigurator.getPostprocessor(),
                highResConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                null,
                null,
                null,
                null,
                lowResConfigurator.getUri(),
                lowResConfigurator.getResizeOptions(),
                lowResConfigurator.getPostprocessor());
    }

    /**
     * @param highResConfigurator     object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator      object wrapping the URL and configurations that will affect the image when it is loaded.
     *                                This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator highResConfigurator,
                     UriConfigurator lowResConfigurator,
                     DrawableConfigurator placeholderConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(highResConfigurator.getUri(),
                highResConfigurator.getResizeOptions(),
                highResConfigurator.getPostprocessor(),
                highResConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                placeholderConfigurator.getResId(),
                placeholderConfigurator.getScaleType(),
                null,
                null,
                lowResConfigurator.getUri(),
                lowResConfigurator.getResizeOptions(),
                lowResConfigurator.getPostprocessor());
    }

    /**
     * @param highResConfigurator     object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator      object wrapping the URL and configurations that will affect the image when it is loaded.
     *                                This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param errorConfigurator       object configuring the error state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    public void load(UriConfigurator highResConfigurator,
                     UriConfigurator lowResConfigurator,
                     DrawableConfigurator placeholderConfigurator,
                     DrawableConfigurator errorConfigurator,
                     ViewConfigurator viewConfigurator
    ) {
        load(highResConfigurator.getUri(),
                highResConfigurator.getResizeOptions(),
                highResConfigurator.getPostprocessor(),
                highResConfigurator.getScaleType(),
                viewConfigurator.getCircularImageOptions(),
                viewConfigurator.getCallbacks(),
                viewConfigurator.getImageView(),
                viewConfigurator.getFadeDuration(),
                viewConfigurator.isDefaultPlaceholder(),
                placeholderConfigurator.getResId(),
                placeholderConfigurator.getScaleType(),
                errorConfigurator.getResId(),
                errorConfigurator.getScaleType(),
                lowResConfigurator.getUri(),
                lowResConfigurator.getResizeOptions(),
                lowResConfigurator.getPostprocessor());
    }


    /**
     * Complete method that configures the image loading using the provided parameters and handles it.
     *
     * @param uri
     * @param CustomPostprocessor
     * @param androidActualImageScaleType
     * @param callbacks
     * @param container
     * @param placeHolderId
     * @param androidPlaceholderScaleType
     * @param errorId
     * @param androidErrorScaleType
     * @param isApplyDefaultPlaceHolder
     * @param lowResUri
     * @param CustomLowResPostProcessor
     */
    private static void load(@Nullable final Uri uri,
                             @Nullable CustomResizeOptions CustomResizeOptions,
                             @Nullable CustomBasePostprocessor CustomPostprocessor,
                             @Nullable ImageView.ScaleType androidActualImageScaleType,
                             @Nullable CustomCircularImageOptions circularImageOptions,
                             @Nullable final CustomImageCallback callbacks,
                             @NonNull final SimpleDraweeView container,
                             @Nullable Integer fadeDuration,
                             boolean isApplyDefaultPlaceHolder,
                             @Nullable Integer placeHolderId,
                             @Nullable ImageView.ScaleType androidPlaceholderScaleType,
                             @Nullable Integer errorId,
                             @Nullable ImageView.ScaleType androidErrorScaleType,
                             @Nullable Uri lowResUri,
                             @Nullable CustomResizeOptions CustomLowResResizeOptions,
                             @Nullable CustomBasePostprocessor CustomLowResPostProcessor) {

        /*
            Converting generic types to specific Fresco types used in this service implementation
         */
        ScalingUtils.ScaleType actualImageScaleType = getFrescoScaleType(androidActualImageScaleType, container);
        ScalingUtils.ScaleType errorScaleType = getFrescoScaleType(androidErrorScaleType, container);
        ScalingUtils.ScaleType placeholderScaleType = getFrescoScaleType(androidPlaceholderScaleType, container);

        Postprocessor postprocessor = getFrescoPostprocessor(CustomPostprocessor);
        Postprocessor lowResPostProcessor = getFrescoPostprocessor(CustomLowResPostProcessor);

        ResizeOptions resizeOptions = getFrescoResizeOptions(CustomResizeOptions);
        ResizeOptions lowResResizeOptions = getFrescoResizeOptions(CustomLowResResizeOptions);

        RoundingParams roundingParams = getFrescoRoundingParams(circularImageOptions);


        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                if (callbacks != null) {
                    callbacks.onImageSuccess();
                }
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                if (callbacks != null) {
                    callbacks.onImageFailure(throwable);
                }
            }
        };


        /*
            Basic configuration in our Drawee View
         */

        if (actualImageScaleType != null) {
            container.getHierarchy().setActualImageScaleType(actualImageScaleType);
        }
        if (errorId != null && errorId != 0
                && !errorId.equals(placeHolderId)) { // We check this because Fresco's default behavior if no error drawable is set, is to use the placeholder. And if the error drawable is set, some things do not work: https://github.com/facebook/fresco/issues/160
            if (errorScaleType == null) {
                container.getHierarchy().setFailureImage(errorId);
            } else {
                container.getHierarchy().setFailureImage(errorId, errorScaleType);
            }
        }
        if (fadeDuration != null) {
            container.getHierarchy().setFadeDuration(fadeDuration);
        }
        if (isApplyDefaultPlaceHolder) {
            if (placeholderScaleType == null) {
                container.getHierarchy().setPlaceholderImage(DEFAULT_IMAGE_PLACEHOLDER_ID);
            } else {
                container.getHierarchy().setPlaceholderImage(DEFAULT_IMAGE_PLACEHOLDER_ID, placeholderScaleType);
            }
        } else if (placeHolderId != null && placeHolderId != 0) {
            if (placeholderScaleType == null) {
                container.getHierarchy().setPlaceholderImage(placeHolderId);
            } else {
                container.getHierarchy().setPlaceholderImage(placeHolderId, placeholderScaleType);
            }
        }

        if (roundingParams != null) {
            container.getHierarchy().setRoundingParams(roundingParams);
        }


        /*
            Decoder configuration. This decoder will be used in both requests.
         */
        ImageDecodeOptions decodeOptions = ImageDecodeOptions.newBuilder()
                .setBitmapConfig(Bitmap.Config.RGB_565)
                .build();

        /*
            Here we create a Request Builder which will handle some image-loading configuration for the full-res image.
         */
        ImageRequest request = null;
        if (uri != null) {
            ImageRequestBuilder requestBuilder = ImageRequestBuilder
                    .newBuilderWithSource(uri)
                    .setImageDecodeOptions(decodeOptions)
                    .setLocalThumbnailPreviewsEnabled(true)
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .setProgressiveRenderingEnabled(true);
            if (postprocessor != null) {
                requestBuilder.setPostprocessor(postprocessor);
            }
            if (resizeOptions != null) {
                requestBuilder.setResizeOptions(resizeOptions);
            }
            request = requestBuilder.build();
        }

         /*
            Here we create a Request Builder which will handle some image-loading configuration for the low-res image.
            This is only used if there is a low-res image provided.
         */
        ImageRequest lowResRequest = null;
        if (lowResUri != null) {
            ImageRequestBuilder lowResRequestBuilder = ImageRequestBuilder
                    .newBuilderWithSource(lowResUri)
                    .setImageDecodeOptions(decodeOptions)
                    .setLocalThumbnailPreviewsEnabled(true)
                    .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                    .setProgressiveRenderingEnabled(true);
            if (lowResPostProcessor != null) {
                lowResRequestBuilder.setPostprocessor(lowResPostProcessor);
            }
            if (lowResResizeOptions != null) {
                lowResRequestBuilder.setResizeOptions(lowResResizeOptions);
            }
            lowResRequest = lowResRequestBuilder.build();
        }


        /*
            Here we create the controller, aggregating everything that has been done before and some other settings, and set the controller to our view.
         */
        PipelineDraweeControllerBuilder builder = Fresco.newDraweeControllerBuilder();
        if (lowResRequest != null) {
            builder.setLowResImageRequest(lowResRequest);
        }
        if (request != null) {
            builder.setImageRequest(request);
        }
        builder.setTapToRetryEnabled(false)
                .setControllerListener(controllerListener)
                .setOldController(container.getController())
                .setAutoPlayAnimations(true);
        DraweeController controller = builder.build();

        container.setController(controller);
    }

    private static RoundingParams getFrescoRoundingParams(CustomCircularImageOptions circularImageOptions) {
        if (circularImageOptions == null) {
            return null;
        }
        RoundingParams roundingParams = RoundingParams.asCircle();
        roundingParams.setBorder(circularImageOptions.getBoarderColor(), circularImageOptions.getBorderWidth());
        roundingParams.setPadding(circularImageOptions.getImagePadding());
        return roundingParams;
    }

    private static ResizeOptions getFrescoResizeOptions(CustomResizeOptions CustomResizeOptions) {
        if (CustomResizeOptions == null) {
            return null;
        }
        return new ResizeOptions(CustomResizeOptions.getWidth(), CustomResizeOptions.getHeight());
    }

    private static ScalingUtils.ScaleType getFrescoScaleType(ImageView.ScaleType scaleType, SimpleDraweeView simpleDraweeView) {
        if (scaleType == null) {
            if (simpleDraweeView != null) {
                if (simpleDraweeView.getScaleType() != null) {
                    scaleType = simpleDraweeView.getScaleType();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
        switch (scaleType) {
            case CENTER:
                return ScalingUtils.ScaleType.CENTER;
            case CENTER_CROP:
                return ScalingUtils.ScaleType.CENTER_CROP;
            case CENTER_INSIDE:
                return ScalingUtils.ScaleType.CENTER_INSIDE;
            case FIT_CENTER:
                return ScalingUtils.ScaleType.FIT_CENTER;
            case FIT_END:
                return ScalingUtils.ScaleType.FIT_END;
            case FIT_START:
                return ScalingUtils.ScaleType.FIT_START;
            case FIT_XY:
                return ScalingUtils.ScaleType.FIT_XY;
            case MATRIX:
                return ScalingUtils.ScaleType.FIT_XY;
            default:
                return null;
        }
    }

    @Override
    public void loadGif(UriConfigurator uriConfigurator, ViewConfigurator viewConfigurator) {
        load(uriConfigurator, viewConfigurator);
    }

    private static Postprocessor getFrescoPostprocessor(CustomBasePostprocessor CustomBasePostprocessor) {
        if (CustomBasePostprocessor instanceof CustomBlurPostprocessor) {
            CustomBlurPostprocessor post = ((CustomBlurPostprocessor) CustomBasePostprocessor);
            return new ScalingBlurPostprocessor(post.getBlurRadius(), post.getBlurIterations(), post.getScaleRatio());
        } else if (CustomBasePostprocessor instanceof CustomMultiplyPostprocessor) {
            CustomMultiplyPostprocessor post = ((CustomMultiplyPostprocessor) CustomBasePostprocessor);

            return new MultiplyPostprocessor(post.getColor());
        }
        return null;
    }

    public void prefetchImage(UriConfigurator uriConfigurator, CacheLevel cacheLevel) {
        if (uriConfigurator != null) {
            Uri uri = uriConfigurator.getUri();
            Postprocessor postprocessor = getFrescoPostprocessor(uriConfigurator.getPostprocessor());
            ResizeOptions resizeOptions = getFrescoResizeOptions(uriConfigurator.getResizeOptions());

        /*
            Decoder configuration.
         */
            ImageDecodeOptions decodeOptions = ImageDecodeOptions.newBuilder()
                    .setBitmapConfig(Bitmap.Config.RGB_565)
                    .build();

        /*
            Here we create a Request Builder which will handle some image-loading configuration for the full-res image.
         */
            ImageRequest request = null;
            if (uri != null) {
                ImageRequestBuilder requestBuilder = ImageRequestBuilder
                        .newBuilderWithSource(uri)
                        .setImageDecodeOptions(decodeOptions)
                        .setLocalThumbnailPreviewsEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .setProgressiveRenderingEnabled(true);
                if (postprocessor != null) {
                    requestBuilder.setPostprocessor(postprocessor);
                }
                if (resizeOptions != null) {
                    requestBuilder.setResizeOptions(resizeOptions);
                }
                request = requestBuilder.build();
            }
            if (cacheLevel != null && cacheLevel == CacheLevel.BitmapCache) {
                Fresco.getImagePipeline().prefetchToBitmapCache(request, App.app);
            } else {
                Fresco.getImagePipeline().prefetchToDiskCache(request, App.app);
            }
        }
    }

    /**
     * Call from Application class.
     *
     * @param level {@link ComponentCallbacks2}
     */
    public void onTrimMemory(final int level) {

    }


}
