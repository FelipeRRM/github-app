package feliperrm.com.githubclient.base.bind.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.BaseObservable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.RecyclerView


/**
 * A fully generic adapter that can be used as is when using Android data binding. View holders will
 * also be fully generic and automatically generated and created. You only need to supply view models
 * and view layouts that bind to the models.
 *
 * Created by FelipeRRM on 20/11/2018.
 */
class DataBindingAdapter
/**
 * @param viewModels They need to extend DataBindingAdapter.LayoutViewModel and the layouts files MUST have a variable called viewModel and be of the subtype of the LayoutViewModel.
 * @param lifecycleOwner Optional Life Cycle Owner (Ex. Fragment or Activity). Mandatory in case your View Models use Live Data and you want the layout to update when the data changes.
 * @param callback Optional implementation of the interface that allows for implementation of custom behaviors outside the adapter.
 */
    (
    private val viewModels: ObservableList<out LayoutViewModel>,
    private val lifecycleOwner: LifecycleOwner? = null,
    private val callback: DataBindingCallback? = null
) : RecyclerView.Adapter<DataBindingAdapter.ViewHolder>(), LifecycleObserver {

    private val callbackRelay = CallbackRelay(this) as ObservableList.OnListChangedCallback<Nothing>
    private var numViewHolders = 0

    init {
        this.viewModels.addOnListChangedCallback(callbackRelay)
        lifecycleOwner?.lifecycle?.addObserver(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            createGenericBinding(
                parent,
                viewType
            )
        ).apply {
            callback?.onViewHolderCreated(
                this,
                numViewHolders++
            )
        }
    }

    private fun createGenericBinding(parent: ViewGroup, layoutId: Int): ViewDataBinding {
        val layoutInflater = LayoutInflater.from(parent.context)
        return DataBindingUtil.inflate(layoutInflater, layoutId, parent, false)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModels[position], lifecycleOwner)
        callback?.onBind(holder, viewModels[position], position)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.cleanUp()
        super.onViewRecycled(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return viewModels[position].mLayoutId
    }

    override fun getItemCount(): Int {
        return viewModels.size
    }

    open class ViewHolder(protected val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {


        open fun bind(viewModel: LayoutViewModel, lifecycleOwner: LifecycleOwner? = null) {
            binding.setLifecycleOwner(lifecycleOwner)
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
        }

        fun cleanUp() {
            binding.setLifecycleOwner(null)
        }


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        viewModels.removeOnListChangedCallback(callbackRelay)
        lifecycleOwner?.lifecycle?.removeObserver(this)
    }

    abstract class LayoutViewModel
    /**
     * @param layoutId The layout resource ID to be used for this view model. Having it here in
     * the view model is what enables the adapter to be fully generic and usable
     * without sub-classing.
     */
    protected constructor(
        @param:LayoutRes @field:LayoutRes
        val mLayoutId: Int
    ) : BaseObservable()

}
