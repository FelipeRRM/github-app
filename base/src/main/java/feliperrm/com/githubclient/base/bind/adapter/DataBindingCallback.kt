package feliperrm.com.githubclient.base.bind.adapter

/**
 * Created by FelipeRRM on 20/11/2018.
 */
interface DataBindingCallback {
    /*
    This is called as soon as a new View Holder is created.
     */
    fun onViewHolderCreated(viewHolder: DataBindingAdapter.ViewHolder, number: Int)

    /*
     This is called after onBind is called in the adapter
      */
    fun onBind(holder: DataBindingAdapter.ViewHolder, viewModel: DataBindingAdapter.LayoutViewModel, position: Int)
}

/**
 * Class with default empty implementations allowing the developer to override just the necessary methods when defining a custom behavior.
 */
open class DataBindingCallbackImp : DataBindingCallback {
    override fun onViewHolderCreated(viewHolder: DataBindingAdapter.ViewHolder, number: Int) {}

    override fun onBind(
        holder: DataBindingAdapter.ViewHolder,
        viewModel: DataBindingAdapter.LayoutViewModel,
        position: Int
    ) {
    }
}