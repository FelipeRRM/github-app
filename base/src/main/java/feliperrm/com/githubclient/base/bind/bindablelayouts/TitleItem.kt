package feliperrm.com.githubclient.base.bind.bindablelayouts

import feliperrm.com.githubclient.R
import feliperrm.com.githubclient.base.bind.adapter.DataBindingAdapter

/**
 * Created by FelipeRRM on 20/11/2018.
 */
class TitleItem : DataBindingAdapter.LayoutViewModel(R.layout.repo_title_item)