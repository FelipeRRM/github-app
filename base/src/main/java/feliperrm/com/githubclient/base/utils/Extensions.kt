package feliperrm.com.githubclient.base.utils

import android.annotation.SuppressLint
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by FelipeRRM on 26/11/2018.
 */

@SuppressLint("SimpleDateFormat")
fun String.fromGitHubDate(format: String): String {
    val dt = DateTime(this)
    val outDf = SimpleDateFormat(format)
    return outDf.format(Date(dt.millis))
}