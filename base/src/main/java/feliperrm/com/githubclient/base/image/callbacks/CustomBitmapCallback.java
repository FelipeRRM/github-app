package feliperrm.com.githubclient.base.image.callbacks;

import android.graphics.Bitmap;

/**
 * Created by felipemagalhaes on 2017-10-23.
 */

public interface CustomBitmapCallback {
    /**
     * Called when the image finishes loading.
     *
     * @param bitmap loaded image.
     */
    public void onBitmapLoaded(Bitmap bitmap);

    /**
     * Called when the load request failed.
     */
    public void onLoadFailed();
}
