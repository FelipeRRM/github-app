package feliperrm.com.githubclient.base.image.postprocessors.fresco;

import android.graphics.*;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import com.facebook.imagepipeline.request.BasePostprocessor;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by bundeeteddee on 2017-10-26.
 * <p>
 * This is a transformation class that does multiply effect on provided bitmap.
 * Can be used for bait brand logo and also to ensure fish species with white background blends seemlessly to the background
 */

public class MultiplyPostprocessor extends BasePostprocessor {

    private final String mColor;

    public MultiplyPostprocessor(String color) {
        mColor = color;
    }

    @Override
    public CloseableReference<Bitmap> process(
            Bitmap sourceBitmap,
            PlatformBitmapFactory bitmapFactory) {

        final CloseableReference<Bitmap> bitmapRef = bitmapFactory.createBitmap(
                sourceBitmap.getWidth(),
                sourceBitmap.getHeight());

        try {
            final Bitmap destBitmap = bitmapRef.get();
            final Canvas canvas = new Canvas(destBitmap);

            Paint paint = new Paint(ANTI_ALIAS_FLAG);
            paint.setColor(Color.parseColor(mColor));
            canvas.drawRect(0, 0, destBitmap.getWidth(), destBitmap.getHeight(), paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
            canvas.drawBitmap(sourceBitmap, 0, 0, paint);

            return CloseableReference.cloneOrNull(bitmapRef);
        } finally {
            CloseableReference.closeSafely(bitmapRef);
        }
    }

}