package feliperrm.com.githubclient.base.image.configurators;

import androidx.annotation.Nullable;
import feliperrm.com.githubclient.base.image.CustomImageView;
import feliperrm.com.githubclient.base.image.callbacks.CustomImageCallback;
import feliperrm.com.githubclient.base.image.options.CustomCircularImageOptions;

/**
 * Created by felipemagalhaes on 2017-09-29.
 * <p>
 * Class wrapping the Custom Image View and extra configurations, like fade duration.
 * A callback can also be defined here, and it will be called when images fail to load or load successfully.
 */

public class ViewConfigurator {
    @Nullable
    private CustomImageView imageView;
    @Nullable
    private Integer fadeDuration;
    @Nullable
    private boolean isDefaultPlaceholder;
    @Nullable
    private CustomCircularImageOptions circularImageOptions;
    @Nullable
    private CustomImageCallback callbacks;

    public ViewConfigurator() {
    }

    public ViewConfigurator(CustomImageView imageView) {
        this.imageView = imageView;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
    }

    public ViewConfigurator(CustomImageView imageView, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.callbacks = callbacks;
    }

    public ViewConfigurator(CustomImageView imageView, boolean isDefaultPlaceholder) {
        this.imageView = imageView;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.callbacks = callbacks;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, boolean isDefaultPlaceholder) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, boolean isDefaultPlaceholder, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
        this.callbacks = callbacks;
    }

    public ViewConfigurator(CustomImageView imageView, CustomCircularImageOptions circularImageOptions) {
        this.imageView = imageView;
        this.circularImageOptions = circularImageOptions;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, CustomCircularImageOptions circularImageOptions) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.circularImageOptions = circularImageOptions;
    }

    public ViewConfigurator(CustomImageView imageView, CustomCircularImageOptions circularImageOptions, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.circularImageOptions = circularImageOptions;
        this.callbacks = callbacks;
    }

    public ViewConfigurator(CustomImageView imageView, boolean isDefaultPlaceholder, CustomCircularImageOptions circularImageOptions) {
        this.imageView = imageView;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
        this.circularImageOptions = circularImageOptions;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, CustomCircularImageOptions circularImageOptions, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.circularImageOptions = circularImageOptions;
        this.callbacks = callbacks;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, boolean isDefaultPlaceholder, CustomCircularImageOptions circularImageOptions) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
        this.circularImageOptions = circularImageOptions;
    }

    public ViewConfigurator(CustomImageView imageView, Integer fadeDuration, boolean isDefaultPlaceholder, CustomCircularImageOptions circularImageOptions, CustomImageCallback callbacks) {
        this.imageView = imageView;
        this.fadeDuration = fadeDuration;
        this.isDefaultPlaceholder = isDefaultPlaceholder;
        this.circularImageOptions = circularImageOptions;
        this.callbacks = callbacks;
    }

    public CustomImageView getImageView() {
        return imageView;
    }

    public void setImageView(CustomImageView imageView) {
        this.imageView = imageView;
    }

    public Integer getFadeDuration() {
        return fadeDuration;
    }

    public void setFadeDuration(Integer fadeDuration) {
        this.fadeDuration = fadeDuration;
    }

    public boolean isDefaultPlaceholder() {
        return isDefaultPlaceholder;
    }

    public void setDefaultPlaceholder(boolean defaultPlaceholder) {
        isDefaultPlaceholder = defaultPlaceholder;
    }

    public CustomImageCallback getCallbacks() {
        return callbacks;
    }

    public void setCallbacks(CustomImageCallback callbacks) {
        this.callbacks = callbacks;
    }

    @Nullable
    public CustomCircularImageOptions getCircularImageOptions() {
        return circularImageOptions;
    }

    public void setCircularImageOptions(@Nullable CustomCircularImageOptions circularImageOptions) {
        this.circularImageOptions = circularImageOptions;
    }
}
