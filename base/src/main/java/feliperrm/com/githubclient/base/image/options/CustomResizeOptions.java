package feliperrm.com.githubclient.base.image.options;

/**
 * Created by felipemagalhaes on 2017-11-01.
 * If set to a request, an instance of this class will force the image to be resized size in memory prior to being decoded.
 * This is very useful when loading really big images on small Views, like camera images into preview views. Saves a lot of time.
 */

public class CustomResizeOptions {

    private int mWidth;
    private int mHeight;

    public CustomResizeOptions(int mWidth, int mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        this.mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        this.mHeight = height;
    }
}
