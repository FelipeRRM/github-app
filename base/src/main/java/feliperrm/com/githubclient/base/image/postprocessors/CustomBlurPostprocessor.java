package feliperrm.com.githubclient.base.image.postprocessors;

/**
 * Created by felipemagalhaes on 2017-10-23.
 */

public class CustomBlurPostprocessor implements CustomBasePostprocessor {

    private int blurRadius;
    private int blurIterations;
    private int scaleRatio;

    public CustomBlurPostprocessor(int blurRadius, int blurIterations, int scaleRatio) {
        this.blurRadius = blurRadius;
        this.blurIterations = blurIterations;
        this.scaleRatio = scaleRatio;
    }

    public int getBlurRadius() {
        return blurRadius;
    }

    public void setBlurRadius(int blurRadius) {
        this.blurRadius = blurRadius;
    }

    public int getBlurIterations() {
        return blurIterations;
    }

    public void setBlurIterations(int blurIterations) {
        this.blurIterations = blurIterations;
    }

    public int getScaleRatio() {
        return scaleRatio;
    }

    public void setScaleRatio(int scaleRatio) {
        this.scaleRatio = scaleRatio;
    }
}
