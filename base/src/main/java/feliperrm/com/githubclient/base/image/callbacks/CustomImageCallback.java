package feliperrm.com.githubclient.base.image.callbacks;

import androidx.annotation.Nullable;

/**
 * Created by felipemagalhaes on 2017-10-23.
 */

public interface CustomImageCallback {

    /**
     * Called when the image has been loaded successfully.
     */
    public void onImageSuccess();

    /**
     * Called when there was an error loading the image.
     *
     * @param throwable detailing what happened. Can be null.
     */
    public void onImageFailure(@Nullable Throwable throwable);
}
