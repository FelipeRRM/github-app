package feliperrm.com.githubclient.base.image.options;

/**
 * Created by Felipe on 12/4/2017.
 */

public class CustomCircularImageOptions {

    private int boarderColor;
    private float borderWidth;
    private float imagePadding;

    public CustomCircularImageOptions(int boarderColor, float borderWidth, float imagePadding) {
        this.boarderColor = boarderColor;
        this.borderWidth = borderWidth;
        this.imagePadding = imagePadding;
    }

    public int getBoarderColor() {
        return boarderColor;
    }

    public void setBoarderColor(int boarderColor) {
        this.boarderColor = boarderColor;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public float getImagePadding() {
        return imagePadding;
    }

    public void setImagePadding(float imagePadding) {
        this.imagePadding = imagePadding;
    }
}
