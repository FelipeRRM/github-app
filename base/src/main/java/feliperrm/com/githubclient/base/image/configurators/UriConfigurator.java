package feliperrm.com.githubclient.base.image.configurators;

import android.net.Uri;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import feliperrm.com.githubclient.base.image.options.CustomResizeOptions;
import feliperrm.com.githubclient.base.image.postprocessors.CustomBasePostprocessor;

/**
 * Created by felipemagalhaes on 2017-09-29.
 * <p>
 * Class wrapping the URL and configurations that will affect the image when it is loaded.
 * An instance of this class holds a Uri with an image to be loaded into a view, along with a scale type
 * and a postprocessor.
 * The scale type and the postprocessor will be applied only when the Uri's image finishes loading.
 * The Resize Options forces the image to be decoded and stored in cache with a specific dimension, very useful for improving performance when loading huge images (like camera images) into small views.
 * <p>
 */


public class UriConfigurator {

    @Nullable
    private Uri uri;
    @Nullable
    private CustomBasePostprocessor postprocessor;
    @Nullable
    private ImageView.ScaleType scaleType;
    @Nullable
    private CustomResizeOptions resizeOptions;

    public UriConfigurator() {
    }

    public UriConfigurator(@Nullable Uri uri) {
        this.uri = uri;
    }

    public UriConfigurator(@Nullable Uri uri, CustomResizeOptions resizeOptions) {
        this.uri = uri;
        this.resizeOptions = resizeOptions;
    }

    public UriConfigurator(@Nullable Uri uri, CustomBasePostprocessor postprocessor) {
        this.uri = uri;
        this.postprocessor = postprocessor;
    }

    public UriConfigurator(@Nullable Uri uri, ImageView.ScaleType scaleType) {
        this.uri = uri;
        this.scaleType = scaleType;
    }

    public UriConfigurator(@Nullable Uri uri, ImageView.ScaleType scaleType, CustomResizeOptions resizeOptions) {
        this.uri = uri;
        this.scaleType = scaleType;
        this.resizeOptions = resizeOptions;
    }

    public UriConfigurator(@Nullable Uri uri, CustomBasePostprocessor postprocessor, ImageView.ScaleType scaleType) {
        this.uri = uri;
        this.postprocessor = postprocessor;
        this.scaleType = scaleType;
    }

    public UriConfigurator(@Nullable Uri uri, CustomBasePostprocessor postprocessor, ImageView.ScaleType scaleType, CustomResizeOptions resizeOptions) {
        this.uri = uri;
        this.postprocessor = postprocessor;
        this.scaleType = scaleType;
        this.resizeOptions = resizeOptions;
    }

    /*
        Same constructors down here, but using a String. Just a more convenient way of using it sometimes.
     */

    public UriConfigurator(@Nullable String uri) {
        this.uri = (uri == null ? null : Uri.parse(uri));
    }

    public UriConfigurator(@Nullable String uri, CustomResizeOptions resizeOptions) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.resizeOptions = resizeOptions;
    }

    public UriConfigurator(@Nullable String uri, CustomBasePostprocessor postprocessor) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.postprocessor = postprocessor;
    }

    public UriConfigurator(@Nullable String uri, ImageView.ScaleType scaleType) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.scaleType = scaleType;
    }

    public UriConfigurator(@Nullable String uri, CustomBasePostprocessor postprocessor, ImageView.ScaleType scaleType) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.postprocessor = postprocessor;
        this.scaleType = scaleType;
    }

    public UriConfigurator(@Nullable String uri, ImageView.ScaleType scaleType, CustomResizeOptions resizeOptions) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.scaleType = scaleType;
        this.resizeOptions = resizeOptions;
    }

    public UriConfigurator(@Nullable String uri, CustomBasePostprocessor postprocessor, ImageView.ScaleType scaleType, CustomResizeOptions resizeOptions) {
        this.uri = (uri == null ? null : Uri.parse(uri));
        this.postprocessor = postprocessor;
        this.scaleType = scaleType;
        this.resizeOptions = resizeOptions;
    }

    @Nullable
    public Uri getUri() {
        return uri;
    }

    public void setUri(@Nullable Uri uri) {
        this.uri = uri;
    }

    @Nullable
    public CustomBasePostprocessor getPostprocessor() {
        return postprocessor;
    }

    public void setPostprocessor(@Nullable CustomBasePostprocessor postprocessor) {
        this.postprocessor = postprocessor;
    }

    @Nullable
    public ImageView.ScaleType getScaleType() {
        return scaleType;
    }

    public void setScaleType(@Nullable ImageView.ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    @Nullable
    public CustomResizeOptions getResizeOptions() {
        return resizeOptions;
    }

    public void setResizeOptions(@Nullable CustomResizeOptions resizeOptions) {
        this.resizeOptions = resizeOptions;
    }
}
