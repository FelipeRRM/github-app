package feliperrm.com.githubclient.base

import android.app.Application
import feliperrm.com.githubclient.base.image.ImageService
import feliperrm.com.githubclient.base.image.implementations.FrescoImageService

/**
 * Created by FelipeRRM on 26/11/2018.
 */
class App : Application() {


    override fun onCreate() {
        super.onCreate()
        app = this
        imageService = FrescoImageService()
        imageService.init(applicationContext)
    }

    companion object {
        lateinit var app: App
        lateinit var imageService: ImageService
    }
}