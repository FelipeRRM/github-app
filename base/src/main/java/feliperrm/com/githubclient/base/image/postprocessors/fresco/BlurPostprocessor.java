package feliperrm.com.githubclient.base.image.postprocessors.fresco;

import android.graphics.Bitmap;
import com.facebook.imagepipeline.nativecode.NativeBlurFilter;
import com.facebook.imagepipeline.request.BasePostprocessor;

/**
 * Applies a blur filter using the {@link NativeBlurFilter#iterativeBoxBlur(Bitmap, int, int)}, but
 * does not down-scale the image beforehand.
 */
public class BlurPostprocessor extends BasePostprocessor {

    private int blurRadius;
    private int blurIterations;

    public BlurPostprocessor(int blurRadius, int blurIterations) {
        this.blurRadius = blurRadius;
        this.blurIterations = blurIterations;
    }

    public int getBlurRadius() {
        return blurRadius;
    }

    public void setBlurRadius(int blurRadius) {
        this.blurRadius = blurRadius;
    }

    public int getBlurIterations() {
        return blurIterations;
    }

    public void setBlurIterations(int blurIterations) {
        this.blurIterations = blurIterations;
    }

    public void process(Bitmap bitmap) {
        NativeBlurFilter.iterativeBoxBlur(bitmap, blurIterations, blurRadius);
    }
}