package feliperrm.com.githubclient.base.image.postprocessors.fresco;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import com.facebook.imagepipeline.nativecode.NativeBlurFilter;

/**
 * Applies a blur filter using the {@link NativeBlurFilter#iterativeBoxBlur(Bitmap, int, int)} and
 * down-scales the bitmap beforehand.
 */
public class ScalingBlurPostprocessor extends BlurPostprocessor {

    /**
     * A scale ratio of 4 means that we reduce the total number of pixels to process by factor 16.
     */
    private int scaleRatio;

    private final Paint mPaint = new Paint();

    public ScalingBlurPostprocessor(int blurRadius, int blurIterations, int scaleRatio) {
        super(blurRadius, blurIterations);
        this.scaleRatio = scaleRatio;
    }

    public int getScaleRatio() {
        return scaleRatio;
    }

    public void setScaleRatio(int scaleRatio) {
        this.scaleRatio = scaleRatio;
    }

    public Paint getmPaint() {
        return mPaint;
    }

    @Override
    public CloseableReference<Bitmap> process(
            Bitmap sourceBitmap,
            PlatformBitmapFactory bitmapFactory) {

        final CloseableReference<Bitmap> bitmapRef = bitmapFactory.createBitmap(
                sourceBitmap.getWidth() / scaleRatio,
                sourceBitmap.getHeight() / scaleRatio);

        try {
            final Bitmap destBitmap = bitmapRef.get();
            final Canvas canvas = new Canvas(destBitmap);

            canvas.drawBitmap(
                    sourceBitmap,
                    null,
                    new Rect(0, 0, destBitmap.getWidth(), destBitmap.getHeight()),
                    mPaint);

            NativeBlurFilter.iterativeBoxBlur(destBitmap, getBlurIterations(), getBlurRadius() / scaleRatio);

            return CloseableReference.cloneOrNull(bitmapRef);
        } finally {
            CloseableReference.closeSafely(bitmapRef);
        }
    }
}