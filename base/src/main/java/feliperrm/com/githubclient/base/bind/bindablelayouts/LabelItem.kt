package feliperrm.com.githubclient.base.bind.bindablelayouts

import feliperrm.com.githubclient.R
import feliperrm.com.githubclient.base.bind.adapter.DataBindingAdapter

/**
 * Created by FelipeRRM on 20/11/2018.
 */
class LabelItem : DataBindingAdapter.LayoutViewModel(R.layout.label_item)