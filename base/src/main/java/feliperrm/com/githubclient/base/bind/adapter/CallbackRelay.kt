package feliperrm.com.githubclient.base.bind.adapter

import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by FelipeRRM on 20/11/2018.
 */
class CallbackRelay(private val adapter: RecyclerView.Adapter<*>) : ObservableList.OnListChangedCallback<ObservableList<out DataBindingAdapter.LayoutViewModel>>() {

    override fun onChanged(layoutViewModels: ObservableList<out DataBindingAdapter.LayoutViewModel>) {
        adapter.notifyDataSetChanged()
    }

    override fun onItemRangeChanged(layoutViewModels: ObservableList<out DataBindingAdapter.LayoutViewModel>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeChanged(positionStart, itemCount)
    }

    override fun onItemRangeInserted(layoutViewModels: ObservableList<out DataBindingAdapter.LayoutViewModel>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeInserted(positionStart, itemCount)
    }

    override fun onItemRangeMoved(layoutViewModels: ObservableList<out DataBindingAdapter.LayoutViewModel>, fromPosition: Int, toPosition: Int, itemCount: Int) {
        throw UnsupportedOperationException()
    }

    override fun onItemRangeRemoved(layoutViewModels: ObservableList<out DataBindingAdapter.LayoutViewModel>, positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeRemoved(positionStart, itemCount)
    }

}