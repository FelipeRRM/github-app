package feliperrm.com.githubclient.base.image;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import feliperrm.com.githubclient.base.image.callbacks.CustomBitmapCallback;
import feliperrm.com.githubclient.base.image.configurators.DrawableConfigurator;
import feliperrm.com.githubclient.base.image.configurators.UriConfigurator;
import feliperrm.com.githubclient.base.image.configurators.ViewConfigurator;

/**
 * Created by felipemagalhaes on 2017-10-23.
 */

public interface ImageService {

    /**
     * Inits Image Loading. This should be only called once!
     *
     * @param context
     */
    void init(Context context);

    /**
     * Loads a resource into a Custom Image View.
     *
     * @param drawableConfigurator
     * @param container
     */
    void load(DrawableConfigurator drawableConfigurator, CustomImageView container);

    /**
     * Loads a resource into a Custom Image View, with additional configurable options.
     *
     * @param drawableConfigurator
     * @param viewConfigurator
     */
    void load(DrawableConfigurator drawableConfigurator, ViewConfigurator viewConfigurator);

    /**
     * Loads an image from an URL and invokes the provided callback.
     *
     * @param context
     * @param url
     * @param callback
     */
    void load(Context context, String url, CustomBitmapCallback callback);

    /**
     * Loads an image from an URI and invokes the provided callback.
     *
     * @param context
     * @param uri
     * @param callback
     */
    void load(Context context, Uri uri, CustomBitmapCallback callback);

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View.
     *
     * @param imageConfigurator object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param viewConfigurator  object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator imageConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View, determining the behavior of the placeholder and the error state.
     *
     * @param imageConfigurator       object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator imageConfigurator,
              DrawableConfigurator placeholderConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * Loads an image from a provided URL with optional configurations into a Simple Drawee View, determining the behavior of the placeholder and the error state.
     *
     * @param imageConfigurator       object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param errorConfigurator       object configuring the error state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator imageConfigurator,
              DrawableConfigurator placeholderConfigurator,
              DrawableConfigurator errorConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * @param highResConfigurator object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator  object wrapping the URL and configurations that will affect the image when it is loaded.
     *                            This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param viewConfigurator    object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator highResConfigurator,
              UriConfigurator lowResConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * @param highResConfigurator     object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator      object wrapping the URL and configurations that will affect the image when it is loaded.
     *                                This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator highResConfigurator,
              UriConfigurator lowResConfigurator,
              DrawableConfigurator placeholderConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * @param highResConfigurator     object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param lowResConfigurator      object wrapping the URL and configurations that will affect the image when it is loaded.
     *                                This should be a low resolution URL to serve as a placeholder before the high resolution URL loads.
     * @param placeholderConfigurator object configuring the placeholder state of the view, setting the image resource and scaling type.
     * @param errorConfigurator       object configuring the error state of the view, setting the image resource and scaling type.
     * @param viewConfigurator        object wrapping the Simple Drawee View and extra things, like fade duration and callbacks.
     */
    void load(UriConfigurator highResConfigurator,
              UriConfigurator lowResConfigurator,
              DrawableConfigurator placeholderConfigurator,
              DrawableConfigurator errorConfigurator,
              ViewConfigurator viewConfigurator
    );

    /**
     * Prefetches the image and its configurations as provided in the URI configurator and stores it in the provided cache level.
     *
     * @param uriConfigurator object wrapping the URL and configurations that will affect the image when it is loaded.
     * @param cacheLevel      where the image should be loaded. If on disk, will use less CPU for pre fetching, but when needed, it will have to be decoded.
     */
    void prefetchImage(UriConfigurator uriConfigurator, CacheLevel cacheLevel);

    /**
     * Call from Application class.
     *
     * @param level {@link ComponentCallbacks2}
     */
    void onTrimMemory(final int level);

    /**
     * Loads a gif image
     *
     * @param uriConfigurator
     * @param viewConfigurator
     */
    void loadGif(UriConfigurator uriConfigurator, ViewConfigurator viewConfigurator);

    /**
     * Enum used to specify the cache level desired for any particular operation
     */
    enum CacheLevel {
        Disk,
        BitmapCache
    }


}
