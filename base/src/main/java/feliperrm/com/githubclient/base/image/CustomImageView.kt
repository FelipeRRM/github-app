package feliperrm.com.githubclient.base.image

import android.content.Context
import android.graphics.PointF
import android.util.AttributeSet
import com.facebook.drawee.generic.GenericDraweeHierarchy
import com.facebook.drawee.view.SimpleDraweeView

/**
 * Created by felipemagalhaes on 2017-10-23.
 */
open class CustomImageView : SimpleDraweeView {

    constructor(context: Context, hierarchy: GenericDraweeHierarchy) : super(context, hierarchy)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )

    fun topCrop() {
        val focusPoint = PointF(0.5f, 0f)
        val hierarchy = hierarchy

        hierarchy.setActualImageFocusPoint(focusPoint)
    }
}
