package feliperrm.com.githubclient.presentation.login

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import feliperrm.com.githubclient.base.App
import feliperrm.com.githubclient.base.utils.OneShotEvent
import feliperrm.com.githubclient.data.models.User
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by FelpeRRM on 13/11/2018.
 */
class LoginViewModel(val repo: GitHubRepository) : ViewModel() {

    val loginPressed = MutableLiveData<OneShotEvent<Unit>>()
    val loggedUserId = MutableLiveData<Long>()

    fun fetchAuthenticatedUser() {
        repo.getOwnUser { userId ->
            userId.let { loggedUserId.value = it }

        }
    }

    fun loginGithub(view: View) {
        loginPressed.value = OneShotEvent(Unit)
    }

}