package feliperrm.com.githubclient.presentation.repodetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import feliperrm.com.githubclient.data.repositories.GitHubRepository

/**
 * Created by FelpeRRM on 27/11/2018.
 */
class RepoDetailsViewModel(repositoryId: Long, private val repo: GitHubRepository) : ViewModel() {

    val repository = repo.getRepository(repositoryId)

    val ownerLogin = MutableLiveData<String>()
    val imageUrl = MutableLiveData<String>()
    val ownerName = MutableLiveData<String>()


    fun updateOwner() {
        repository.value?.owner?.login?.let { login ->
            repo.getUserByLogin(login) { user ->
                ownerLogin.value = user.login
                imageUrl.value = user.avatar_url
                ownerName.value = user.name
            }
        }
    }

}