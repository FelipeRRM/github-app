package feliperrm.com.githubclient.presentation.repodetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import feliperrm.com.githubclient.login.R

class RepoDetailsActivity : AppCompatActivity() {

    private val repositoryId: Long by lazy {
        intent?.extras?.getLong(REPO_ID_KEY) ?: throw Exception("A Repository ID must be passed through the bundle!")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details)
        supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout, RepoDetailsFragment.newInstance(repositoryId)).commitNowAllowingStateLoss()
    }

    companion object {
        private const val REPO_ID_KEY = "repoid"
        fun getIntent(repositoryId: Long, context: Context): Intent {
            val intent = Intent(context, RepoDetailsActivity::class.java)
            intent.putExtra(REPO_ID_KEY, repositoryId)
            return intent
        }
    }

}
