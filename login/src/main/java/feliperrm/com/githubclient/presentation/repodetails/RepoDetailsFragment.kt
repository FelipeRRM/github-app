package feliperrm.com.githubclient.presentation.repodetails


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import feliperrm.com.githubclient.base.utils.getViewModel
import feliperrm.com.githubclient.data.DataBase
import feliperrm.com.githubclient.data.repositories.GitHubRemoteDataSource
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.login.databinding.FragmentRepoDetailsBinding


/**
 * Created by FelipeRRM on 27/11/2018.
 *
 * Use the [RepoDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RepoDetailsFragment : Fragment() {

    private val repoId by lazy {
        arguments?.getLong(REPOSITORY_ID_KEY) ?: throw Exception("A Repository ID must be passed through the bundle!")
    }

    private val vm by lazy {
        getViewModel {
            RepoDetailsViewModel(
                repoId,
                GitHubRepository(GitHubRemoteDataSource(), DataBase.instance)
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentRepoDetailsBinding.inflate(layoutInflater, container, false)
            .apply { viewModel = vm; setLifecycleOwner(this@RepoDetailsFragment) }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.repository.observe(this, Observer { vm.updateOwner() })
    }

    companion object {

        private const val REPOSITORY_ID_KEY = "repoidkey"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment RepoDetailsFragment.
         */
        @JvmStatic
        fun newInstance(repoId: Long) =
            RepoDetailsFragment().apply { arguments = Bundle().apply { putLong(REPOSITORY_ID_KEY, repoId) } }
    }
}
