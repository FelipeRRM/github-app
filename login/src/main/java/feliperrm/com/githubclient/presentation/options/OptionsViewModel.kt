package feliperrm.com.githubclient.presentation.options

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.View
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import androidx.lifecycle.ViewModel
import feliperrm.com.githubclient.data.base.Network
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.presentation.login.LoginActivity

/**
 * Created by FelpeRRM on 13/11/2018.
 */
class OptionsViewModel(userId: Long, repo: GitHubRepository) : ViewModel() {

    val user = repo.getUser(userId)

    fun logout(view: View) {
        CookieSyncManager.createInstance(view.context)
        val cookieManager = CookieManager.getInstance()
        cookieManager.removeAllCookies {}
        val PREFERENCE = "github_prefs"
        val sharedPreferences = view.context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE)
        sharedPreferences.edit().remove("oauth_token").apply()
        Network.accessToken = ""
        val intent = Intent(view.context, LoginActivity::class.java)
        intent.flags = FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
        view.context.startActivity(intent)
    }

}