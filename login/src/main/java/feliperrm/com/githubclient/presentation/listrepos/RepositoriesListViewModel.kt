package feliperrm.com.githubclient.presentation.listrepos

import android.os.Handler
import android.view.View
import androidx.databinding.ObservableArrayList
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import feliperrm.com.githubclient.base.bind.adapter.DataBindingAdapter
import feliperrm.com.githubclient.base.bind.adapter.DataBindingCallbackImp
import feliperrm.com.githubclient.base.bind.bindablelayouts.LabelItem
import feliperrm.com.githubclient.base.bind.bindablelayouts.RepositoryItem
import feliperrm.com.githubclient.base.bind.bindablelayouts.TitleItem
import feliperrm.com.githubclient.base.utils.fromGitHubDate
import feliperrm.com.githubclient.data.models.RepositoryModel
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.presentation.repodetails.RepoDetailsActivity

/**
 * Created by FelpeRRM on 13/11/2018.
 */
class RepositoriesListViewModel(val repo: GitHubRepository) : ViewModel() {

    val uiRepositories = MutableLiveData<ObservableArrayList<DataBindingAdapter.LayoutViewModel>>().apply {
        value = ObservableArrayList()
    }
    var isLoading = MutableLiveData<Boolean>().apply { value = true }
    var shouldAnimate = true

    init {
        repo.getUsersOwnRepositories { mapToUiModels(it) }
    }

    private fun mapToUiModels(repositories: List<RepositoryModel>) {
        uiRepositories.value?.add(TitleItem())
        uiRepositories.value?.add(LabelItem())
        uiRepositories.value?.addAll(repositories.map {
            RepositoryItem(
                it.name,
                it.owner?.login,
                it.private ?: true,
                it.updated_at?.fromGitHubDate("MMM dd yyyy"),
                it.created_at?.fromGitHubDate("MMM dd yyyy"),
                it.id
            ) { clickedView, repositoryId ->
                clickedView.context.startActivity(RepoDetailsActivity.getIntent(repositoryId, clickedView.context))
            }
        })
        isLoading.value = false
        Handler().postDelayed({ shouldAnimate = false }, 1000L)
    }

    // Elements entering the screen should decelerate: https://material.io/design/motion/speed.html#easing
    private fun animateViewEntrance(view: View, number: Int) {
        view.alpha = 0f
        view.translationY = 500f
        view.animate().alpha(1f).translationY(0f).setStartDelay(number * 90L).setDuration(180L)
            .setInterpolator(LinearOutSlowInInterpolator()).start()
    }

    val callback = object : DataBindingCallbackImp() {
        override fun onViewHolderCreated(viewHolder: DataBindingAdapter.ViewHolder, number: Int) {
            if (shouldAnimate) {
                animateViewEntrance(viewHolder.itemView, number)
            }
        }
    }

}