package feliperrm.com.githubclient.presentation.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import feliperrm.com.githubclient.base.utils.OneShotEvent
import feliperrm.com.githubclient.base.utils.getViewModel
import feliperrm.com.githubclient.login.databinding.FragmentLoginBinding
import feliperrm.com.githubclient.oauthlib.GithubOauth


/**
 * Created by FelpeRRM on 13/11/2018.
 */

private const val GITHUB_ID = "ec5ee5ee54b87d982c09"
private const val GITHUB_SECRET = "2cdd0c08876ad81d93d2d2c5536dc4abdff1940d"

class LoginFragment : Fragment() {

    private val vm by lazy { activity?.getViewModel<LoginViewModel>() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentLoginBinding.inflate(inflater, container, false)
            .apply { viewModel = vm; setLifecycleOwner(this@LoginFragment) }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm?.loginPressed?.observe(this,
            Observer<OneShotEvent<Unit>> { pressed ->
                pressed?.getContentIfNotHandled()?.let { openGitHubOAuth() }
            })
    }

    private fun openGitHubOAuth() {

        GithubOauth
            .Builder()
            .withClientId(GITHUB_ID)
            .withClientSecret(GITHUB_SECRET)
            .withContext(activity)
            .packageName("feliperrm.com.githubclient.presentation.listrepos")
            .nextActivity("feliperrm.com.githubclient.presentation.listrepos.LoggedUserActivity")
            .debug(true)
            .execute()
    }



}
