package feliperrm.com.githubclient.presentation.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import feliperrm.com.githubclient.base.utils.getViewModel
import feliperrm.com.githubclient.data.DataBase
import feliperrm.com.githubclient.data.base.Network
import feliperrm.com.githubclient.data.repositories.GitHubRemoteDataSource
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.login.R
import feliperrm.com.githubclient.oauthlib.GithubOauth
import feliperrm.com.githubclient.presentation.listrepos.LoggedUserActivity
import timber.log.Timber

/**
 * Created by FelpeRRM on 13/11/2018.
 */
class LoginActivity : AppCompatActivity() {

    private val vm by lazy {
        getViewModel {
            LoginViewModel(
                GitHubRepository(
                    GitHubRemoteDataSource(),
                    DataBase.instance
                )
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, LoginFragment()).commit()
        vm.loggedUserId.observe(this, Observer<Long> {
            finish()
            startActivity(LoggedUserActivity.getIntent(it, this))
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GithubOauth.REQUEST_CODE) {
            val PREFERENCE = "github_prefs"
            val sharedPreferences = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE)
            val oauthToken = sharedPreferences.getString("oauth_token", "")
            Timber.d("Oauth token for github logged in user is :$oauthToken")
            Network.accessToken = oauthToken ?: ""
            if (oauthToken.isNotBlank())
                vm.fetchAuthenticatedUser()
        }
    }
}
