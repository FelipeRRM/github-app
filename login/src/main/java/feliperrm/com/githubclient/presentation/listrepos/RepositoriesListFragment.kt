package feliperrm.com.githubclient.presentation.listrepos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import feliperrm.com.githubclient.base.utils.getViewModel
import feliperrm.com.githubclient.data.DataBase
import feliperrm.com.githubclient.data.repositories.GitHubRemoteDataSource
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.login.databinding.FragmentRepositoriesListBinding


/**
 * Created by FelpeRRM on 13/11/2018.
 */

class RepositoriesListFragment : Fragment() {

    private val vm by lazy {
        getViewModel {
            RepositoriesListViewModel(
                GitHubRepository(
                    GitHubRemoteDataSource(),
                    DataBase.instance
                )
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentRepositoriesListBinding.inflate(inflater, container, false)
            .apply { viewModel = vm; setLifecycleOwner(this@RepositoriesListFragment) }.root
    }

}
