package feliperrm.com.githubclient.presentation.options

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import feliperrm.com.githubclient.base.App
import feliperrm.com.githubclient.base.image.configurators.UriConfigurator
import feliperrm.com.githubclient.base.image.configurators.ViewConfigurator
import feliperrm.com.githubclient.base.image.options.CustomCircularImageOptions
import feliperrm.com.githubclient.base.utils.getViewModel
import feliperrm.com.githubclient.data.DataBase
import feliperrm.com.githubclient.data.repositories.GitHubRemoteDataSource
import feliperrm.com.githubclient.data.repositories.GitHubRepository
import feliperrm.com.githubclient.login.databinding.FragmentOptionsBinding
import kotlinx.android.synthetic.main.fragment_options.*


/**
 * Created by FelipeRRM on 26/11/2018.
 *
 * Use the [OptionsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class OptionsFragment : Fragment() {

    private val userId: Long by lazy {
        arguments?.getLong(USER_ID_KEY) ?: throw Exception("A User ID must be passed through the bundle!")
    }

    private val vm by lazy {
        getViewModel {
            OptionsViewModel(userId, GitHubRepository(GitHubRemoteDataSource(), DataBase.instance))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentOptionsBinding.inflate(inflater, container, false)
            .apply { viewModel = vm; setLifecycleOwner(this@OptionsFragment) }.root
    }

    companion object {
        private const val USER_ID_KEY = "useridkey"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment OptionsFragment.
         */
        @JvmStatic
        fun newInstance(userId: Long) =
            OptionsFragment().apply { arguments = Bundle().apply { putLong(USER_ID_KEY, userId) } }
    }

}
