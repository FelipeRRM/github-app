package feliperrm.com.githubclient.presentation.listrepos

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import feliperrm.com.githubclient.login.R
import feliperrm.com.githubclient.presentation.options.OptionsFragment
import kotlinx.android.synthetic.main.activity_tabs.*

/**
 * Created by FelpeRRM on 13/11/2018.
 */
class LoggedUserActivity : AppCompatActivity() {

    private val userId: Long by lazy {
        intent?.extras?.getLong(USER_ID_KEY) ?: throw Exception("A User ID must be passed through the bundle!")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabs)
        viewPager.adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> RepositoriesListFragment()
                    1 -> OptionsFragment.newInstance(userId)
                    else -> Fragment()
                }
            }

            override fun getCount() = 2

            override fun getPageTitle(position: Int): CharSequence? {
                return when (position) {
                    0 -> getString(R.string.repo_tab_title)
                    1 -> getString(R.string.option_profile_tab_title)
                    else -> ""
                }
            }
        }
        tabLayout.setupWithViewPager(viewPager)
    }

    companion object {
        private const val USER_ID_KEY = "userid"
        fun getIntent(userId: Long, context: Context): Intent {
            val intent = Intent(context, LoggedUserActivity::class.java)
            intent.putExtra(USER_ID_KEY, userId)
            return intent
        }
    }

}
