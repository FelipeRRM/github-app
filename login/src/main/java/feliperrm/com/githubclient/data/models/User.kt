package feliperrm.com.githubclient.data.models

import androidx.lifecycle.LiveData
import androidx.room.*


@Entity
data class User(
    var avatar_url: String? = null,
    var bio: String? = null,
    var blog: String? = null,
    var collaborators: Int? = null,
    var company: String? = null,
    var created_at: String? = null,
    var disk_usage: Int? = null,
    var email: String? = null,
    var events_url: String? = null,
    var followers: Int? = null,
    var followers_url: String? = null,
    var following: Int? = null,
    var following_url: String? = null,
    var gists_url: String? = null,
    var gravatar_id: String? = null,
    var hireable: Boolean? = null,
    var html_url: String? = null,
    @PrimaryKey var id: Long = 0,
    var location: String? = null,
    var login: String? = null,
    var name: String? = null,
    var node_id: String? = null,
    var organizations_url: String? = null,
    var owned_private_repos: Int? = null,
    @Ignore var plan: Plan? = null,
    var private_gists: Int? = null,
    var public_gists: Int? = null,
    var public_repos: Int? = null,
    var received_events_url: String? = null,
    var repos_url: String? = null,
    var site_admin: Boolean? = null,
    var starred_url: String? = null,
    var subscriptions_url: String? = null,
    var total_private_repos: Int? = null,
    var two_factor_authentication: Boolean? = null,
    var type: String? = null,
    var updated_at: String? = null,
    var url: String? = null
)

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long?


    @Query("SELECT * FROM User ORDER BY created_at desc")
    fun fetchAllUsers(): LiveData<List<User>>


    @Query("SELECT * FROM User WHERE id =:userId")
    fun getUser(userId: Long): LiveData<User>

    @Query("SELECT * FROM User WHERE login =:login")
    fun getUserByLogin(login: String): LiveData<User>

    @Update
    fun updateUser(user: User)


    @Delete
    fun deleteUser(user: User)
}

class UserConverter {
    @TypeConverter
    fun fromUsername(value: String?): User? {
        return value?.let { User(login = it) }
    }

    @TypeConverter
    fun userToString(user: User?): String? {
        return user?.login
    }
}