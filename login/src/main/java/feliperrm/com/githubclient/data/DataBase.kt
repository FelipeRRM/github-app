package feliperrm.com.githubclient.data

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import feliperrm.com.githubclient.base.App
import feliperrm.com.githubclient.data.models.*

/**
 * Created by FelipeRRM on 27/11/2018.
 */
@Database(entities = [User::class, RepositoryModel::class], version = 1)
@TypeConverters(UserConverter::class)
abstract class DataBase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun repositoryDao(): RepositoryDao

    companion object {
        val instance: DataBase by lazy { createInstance() }

        private fun createInstance(): DataBase {
            synchronized(DataBase::class) {
                return Room.databaseBuilder(App.app, DataBase::class.java, "githubclient.db").build()
            }
        }

    }

}