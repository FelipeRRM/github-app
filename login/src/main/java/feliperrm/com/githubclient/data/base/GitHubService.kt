package feliperrm.com.githubclient.data.base

import feliperrm.com.githubclient.data.models.RepositoryModel
import feliperrm.com.githubclient.data.models.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * Created by FelipeRRM on 21/11/2018.
 */
interface GitHubService {

    /**
     * Get authenticated user's data
     */
    @GET("/user")
    fun getOwnUser(): Call<User>

    @GET("/users/{userName}")
    fun getUser(@Path("userName") userName: String): Call<User>

    /**
     * Get authenticated user's repositories
     */
    @GET("user/repos")
    fun listOwnRepos(): Call<List<RepositoryModel>>

}