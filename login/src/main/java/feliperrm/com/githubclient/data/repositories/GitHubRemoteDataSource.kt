package feliperrm.com.githubclient.data.repositories

import feliperrm.com.githubclient.data.base.GitHubService
import feliperrm.com.githubclient.data.base.Network
import feliperrm.com.githubclient.data.models.RepositoryModel
import feliperrm.com.githubclient.data.models.User
import retrofit2.Call

/**
 * Created by FelipeRRM on 21/11/2018.
 */
class GitHubRemoteDataSource : GitHubDataSource {

    private val service: GitHubService = Network.retrofit.create(GitHubService::class.java)

    override fun getUsersOwnRepositories(): Call<List<RepositoryModel>> {
        return service.listOwnRepos()
    }

    override fun getOwnUser(): Call<User> {
        return service.getOwnUser()
    }

    override fun getUser(userName: String): Call<User> {
        return service.getUser(userName)
    }

}