package feliperrm.com.githubclient.data.models

/**
 * Created by FelipeRRM on 21/11/2018.
 */
data class Permissions(
    val admin: Boolean?,
    val pull: Boolean?,
    val push: Boolean?
)