package feliperrm.com.githubclient.data.base

import feliperrm.com.githubclient.login.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by FelpeRRM on 14/11/2018.
 */

private const val HOST_URL = "https://api.github.com/"

class Network {

    companion object {

        var accessToken = ""

        private val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor{ chain ->
                val original = chain.request()
                val request = original.newBuilder()
                    .header("User-Agent", "GitHubClient")
                    .header("Accept", "application/vnd.github.v3+json")
                    .header("Authorization", "token $accessToken")
                    .method(original.method(), original.body())
                    .build()
                chain.proceed(request)
            }
            .addInterceptor(HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
            .build()

        val retrofit: Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(HOST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        }
    }


}