package feliperrm.com.githubclient.data.models

/**
 * Created by FelipeRRM on 21/11/2018.
 */
data class License(
    val key: String?,
    val name: String?,
    val node_id: String?,
    val spdx_id: String?,
    val url: String?
)