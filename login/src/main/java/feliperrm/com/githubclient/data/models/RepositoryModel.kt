package feliperrm.com.githubclient.data.models

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Created by FelipeRRM on 21/11/2018.
 */
@Entity
data class RepositoryModel(
    var archive_url: String? = null,
    var archived: Boolean? = null,
    var assignees_url: String? = null,
    var blobs_url: String? = null,
    var branches_url: String? = null,
    var clone_url: String? = null,
    var collaborators_url: String? = null,
    var comments_url: String? = null,
    var commits_url: String? = null,
    var compare_url: String? = null,
    var contents_url: String? = null,
    var contributors_url: String? = null,
    var created_at: String? = null,
    var default_branch: String? = null,
    var deployments_url: String? = null,
    var description: String? = null,
    var downloads_url: String? = null,
    var events_url: String? = null,
    var fork: Boolean? = null,
    var forks_count: Int? = null,
    var forks_url: String? = null,
    var full_name: String? = null,
    var git_commits_url: String? = null,
    var git_refs_url: String? = null,
    var git_tags_url: String? = null,
    var git_url: String? = null,
    var has_downloads: Boolean? = null,
    var has_issues: Boolean? = null,
    var has_pages: Boolean? = null,
    var has_projects: Boolean? = null,
    var has_wiki: Boolean? = null,
    var homepage: String? = null,
    var hooks_url: String? = null,
    var html_url: String? = null,
    @PrimaryKey var id: Long = 0L,
    var issue_comment_url: String? = null,
    var issue_events_url: String? = null,
    var issues_url: String? = null,
    var keys_url: String? = null,
    var labels_url: String? = null,
    @Ignore var language: Any? = null,
    var languages_url: String? = null,
    @Ignore var license: License? = null,
    var merges_url: String? = null,
    var milestones_url: String? = null,
    var mirror_url: String? = null,
    var name: String? = null,
    var network_count: Int? = null,
    var node_id: String? = null,
    var notifications_url: String? = null,
    var open_issues_count: Int? = null,
    var owner: User? = null,
    @Ignore var permissions: Permissions? = null,
    var `private`: Boolean? = null,
    var pulls_url: String? = null,
    var pushed_at: String? = null,
    var releases_url: String? = null,
    var size: Int? = null,
    var ssh_url: String? = null,
    var stargazers_count: Int? = null,
    var stargazers_url: String? = null,
    var statuses_url: String? = null,
    var subscribers_count: Int? = null,
    var subscribers_url: String? = null,
    var subscription_url: String? = null,
    var svn_url: String? = null,
    var tags_url: String? = null,
    var teams_url: String? = null,
    @Ignore var topics: List<String?>? = null,
    var trees_url: String? = null,
    var updated_at: String? = null,
    var url: String? = null,
    var watchers_count: Int? = null
)

@Dao
interface RepositoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepository(repository: RepositoryModel): Long?


    @Query("SELECT * FROM RepositoryModel ORDER BY created_at desc")
    fun fetchAllRepositoryModels(): LiveData<List<RepositoryModel>>


    @Query("SELECT * FROM RepositoryModel WHERE id =:repositoryId")
    fun getRepositoryModel(repositoryId: Long): LiveData<RepositoryModel>


    @Update
    fun updateRepositoryModel(repository: RepositoryModel)


    @Delete
    fun deleteRepositoryModel(repository: RepositoryModel)
}