package feliperrm.com.githubclient.data.repositories

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import feliperrm.com.githubclient.data.DataBase
import feliperrm.com.githubclient.data.models.RepositoryModel
import feliperrm.com.githubclient.data.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

/**
 * Created by FelipeRRM on 21/11/2018.
 */
class GitHubRepository(private val remote: GitHubDataSource, private val db: DataBase) {

    fun getUsersOwnRepositories(callback: (repositores: List<RepositoryModel>) -> Unit) {
        remote.getUsersOwnRepositories().enqueue(object : Callback<List<RepositoryModel>> {
            override fun onFailure(call: Call<List<RepositoryModel>>, t: Throwable) = Timber.e(t)

            override fun onResponse(call: Call<List<RepositoryModel>>, response: Response<List<RepositoryModel>>) {
                response.body()?.let { repositories ->
                    AddRepositoriesAsyncTask(db).execute(*repositories.toTypedArray())
                    callback.invoke(repositories)
                }
            }

        })
    }

    fun getOwnUser(callback: (userId: Long?) -> Unit) {
        remote.getOwnUser().enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) = Timber.e(t)

            override fun onResponse(call: Call<User>, response: Response<User>) {
                AddUsersAsyncTask(db).execute(response.body())
                callback.invoke(response.body()?.id)
            }
        })
    }

    fun getUser(userId: Long): LiveData<User> {
        return db.userDao().getUser(userId)
    }

    fun getRepository(repositoryId: Long): LiveData<RepositoryModel> {
        return db.repositoryDao().getRepositoryModel(repositoryId)
    }

    fun getUserByLogin(login: String, callback: (user: User) -> Unit) {
        remote.getUser(login).enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) = Timber.e(t)

            override fun onResponse(call: Call<User>, response: Response<User>) {
                AddUsersAsyncTask(db).execute(response.body())
                response.body()?.let{callback(it)}
            }

        })
    }

    class AddUsersAsyncTask(private val db: DataBase) : AsyncTask<User, Unit, Unit>() {
        override fun doInBackground(vararg users: User?) {
            users.forEach { it?.let { user -> db.userDao().insertUser(user) } }
        }
    }

    class AddRepositoriesAsyncTask(private val db: DataBase) : AsyncTask<RepositoryModel, Unit, Unit>() {
        override fun doInBackground(vararg repositories: RepositoryModel?) {
            repositories.forEach { it?.let { repository -> db.repositoryDao().insertRepository(repository) } }
        }
    }

}