package feliperrm.com.githubclient.data.repositories

import feliperrm.com.githubclient.data.models.RepositoryModel
import feliperrm.com.githubclient.data.models.User
import retrofit2.Call

/**
 * Created by FelipeRRM on 21/11/2018.
 */
interface GitHubDataSource {
    fun getUsersOwnRepositories(): Call<List<RepositoryModel>>

    fun getOwnUser(): Call<User>

    fun getUser(userName: String): Call<User>
}